# AnyKernel2 Ramdisk Mod Script
# osm0sis @ xda-developers

## AnyKernel setup
# begin properties
properties() {
kernel.string=void kernel
do.devicecheck=1
do.modules=1
do.cleanup=1
do.cleanuponabort=0
device.name1=t0lte
device.name2=t0ltecan
device.name3=SGH-I317
device.name4=SGH-I317M
device.name5=
} # end properties

# shell variables
block=/dev/block/platform/dw_mmc/by-name/BOOT;
is_slot_device=0;


## AnyKernel methods (DO NOT CHANGE)
# import patching functions/variables - see for reference
. /tmp/anykernel/tools/ak2-core.sh;


## AnyKernel permissions
# set permissions for included ramdisk files
chmod -R 755 $ramdisk
chmod 644 $ramdisk/sbin/media_profiles.xml


## AnyKernel install
dump_boot;

# begin ramdisk changes

# begin system changes
mount -o remount,rw /system

# check if this version need kernel modules
mod=/tmp/anykernel/modules
if [ -z $(find /tmp/anykernel/modules -name "*.ko" 2>/dev/null) ]; then
  sed -i 's/do.modules=1/do.modules=0/g' /tmp/anykernel/anykernel.sh # disable modules injection
fi

# fstab.smdk4x12
# get android version in a three digit number
av=$(grep ro.build.version.release /system/build.prop | cut -d '=' -f 2 | sed 's/\.//g' 2>/dev/null)

# system is not installed or build.prop file is not available
if [ -z "${av}" ]; then
  abort
fi

# Android < 7.0 (Android 6.0)
if [ "${av}" -lt "700" ]; then
  sed -i '/OTA/d' fstab.smdk4x12
  sed -i 's/voldmanaged=usb:auto,noemulatedsd/voldmanaged=usb:auto,noemulatedsd,encryptable=userdata/g' fstab.smdk4x12
fi

# Android < 6.0 (Android 5.0|5.1)
if [ "${av}" -lt "600" ]; then
  sed -i 's|s3c-sdhci.2/mmc_host/mmc1|s3c-sdhci.2/mmc_host|g' fstab.smdk4x12
  sed -i 's|/storage/sdcard1|auto|g' fstab.smdk4x12
  sed -i 's/voldmanaged=sdcard1:auto,encryptable=userdata/voldmanaged=sdcard1:auto,noemulatedsd/g' fstab.smdk4x12
  sed -i 's|/storage/usbdisk0|auto|g' fstab.smdk4x12
  sed -i 's/voldmanaged=usb:auto,noemulatedsd,encryptable=userdata/voldmanaged=usbdisk0:auto/g' fstab.smdk4x12
fi

# init.smdk4x12.rc
remove_section init.smdk4x12.rc "on early-init" "noop"
remove_section init.smdk4x12.rc "on charger" "powersave"
remove_section init.smdk4x12.rc "on property:sys.boot_completed=1" "row"
remove_section init.smdk4x12.rc "# Powersave" "performance"
remove_line init.smdk4x12.rc "write /sys/class/mdnie/mdnie/scenario 0"
remove_line init.smdk4x12.rc "write /sys/class/mdnie/mdnie/mode 0"

# remove older modules
rm -rf lib/modules
rm -rf /system/lib/modules
mkdir -p /system/lib/modules

# busybox for synapse
sysbb=/system/xbin/busybox
if [ ! -f "${sysbb}" ]; then
  cp /tmp/anykernel/tools/busybox "${sysbb}"
  chmod +x "${sysbb}"
fi

# synapse support
append_file init.smdk4x12.rc "/sbin/uci" synapse

# end system changes
mount -o remount,ro /system

# end ramdisk changes

write_boot;

## end install

